/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.jaxb;

import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import ru.edance.onlinepbx.jaxb.model.WidgetItems;

/**
 *
 * @author ILMIR
 */
public final class UnmarshalHelper {

    public static WidgetItems unmarshal(InputStream inStrem) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(WidgetItems.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        // output pretty printed
        return (WidgetItems) jaxbUnmarshaller.unmarshal(inStrem);
       
    }
}
    
