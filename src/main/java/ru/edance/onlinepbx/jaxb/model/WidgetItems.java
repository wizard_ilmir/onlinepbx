/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.jaxb.model;

import ru.edance.onlinepbx.jaxb.model.WidgetItem;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ILMIR
 */
@XmlRootElement(name = "WidgetItems")
@XmlAccessorType (XmlAccessType.FIELD)
public class WidgetItems {

    @XmlElement(name = "WidgetItem")
    private List<WidgetItem> widgetItems = null;

    public List<WidgetItem> getWidgetItems() {
        return widgetItems;
    }

    public void setWidgetItems(List<WidgetItem> widgetItems) {
        this.widgetItems = widgetItems;
    }

    @Override
    public String toString() {
        if (widgetItems == null) {
            return null;
        } else if (widgetItems.isEmpty()) {
            return "WidgetItems is empty";
        }
        StringBuffer strBuf = new StringBuffer();
        boolean isFirstItem = true;

        for (WidgetItem item : widgetItems) {
            if (isFirstItem) {
                strBuf.append(item.toString());
                isFirstItem = false;
            } else {
                strBuf.append("\n" + item.toString());
            }

        }
        return strBuf.toString();
    }

}
