/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.jaxb.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ILMIR
 */
@XmlRootElement(name = "WidgetItem")
@XmlAccessorType (XmlAccessType.FIELD)
public class WidgetItem {
    
    private String widgetKey = "";
    private String num = "";;
    private String type = "";;
    private long numbersOfday;

    public WidgetItem() {

    }

    public WidgetItem(String widgetKey, String num) {
        this.widgetKey = widgetKey;
        this.num = num;
    }

    public String getWidgetKey() {
        return widgetKey;
    }

    public void setWidgetKey(String widgetKey) {
        this.widgetKey = widgetKey;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getNumbersOfday() {
        return numbersOfday;
    }

    public void setNumbersOfday(int numbersOfday) {
        this.numbersOfday = numbersOfday;
    }

    @Override
    public String toString() {
        return "{ widgetKey:" + widgetKey + " num:" + num + " type:" + type + " }";
    }

    
    
}
