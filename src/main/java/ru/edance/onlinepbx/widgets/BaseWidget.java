/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.widgets;

import nl.pvanassen.geckoboard.api.Push;
import ru.edance.onlinepbx.OnlinePBXConnector;
import ru.edance.onlinepbx.jaxb.model.WidgetItem;

/**
 *
 * @author ILMIR
 */
public abstract class BaseWidget {

    public static Push factoryWidget(OnlinePBXConnector connector, WidgetItem widgetItem) {
        if (widgetItem.getType().equals("geckOMeter")) {
            return new DerivedGeckOMeterin(connector).getPushObject(widgetItem);
        } else if (widgetItem.getType().equals("RAGNumbers")) {
            return new DerivedRAGNumbers(connector).getPushObject(widgetItem);
        } else {
            return null;
        }

    }

    protected Push getPushObject(WidgetItem widgetItem) {
        throw new IllegalArgumentException("Method getPushObject is not implemented.");

    }

}
