/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.widgets;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.pvanassen.geckoboard.api.Geckoboard;
import nl.pvanassen.geckoboard.api.Push;
import nl.pvanassen.geckoboard.api.json.common.GraphType;
import nl.pvanassen.geckoboard.api.widget.GeckOMeter;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import ru.edance.onlinepbx.OnlinePBXConnector;
import ru.edance.onlinepbx.jaxb.model.WidgetItem;
import ru.edance.onlinepbx.utils.ResourseHelper;

/**
 *
 * @author ILMIR
 */
public class DerivedGeckOMeterin extends BaseWidget {

    private OnlinePBXConnector connector;
    private static String urlMethod = "/history/search.json";
    private static Properties geckOMeterProps;

    public DerivedGeckOMeterin() {
        
    }

    public DerivedGeckOMeterin(OnlinePBXConnector connector) {
        this.connector = connector;
       
    }

    @Override
    public Push getPushObject(WidgetItem widgetItem) {
        initPros();

        ObjectNode params = connector.getObjectMapper().valueToTree(new Object());
        params.put("billsec_from", geckOMeterProps.getProperty("billsec_from"));
        params.put("date_from", ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS).format(DateTimeFormatter.RFC_1123_DATE_TIME));
        params.put("date_to", ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS).plusSeconds(86400 - 1).format(DateTimeFormatter.RFC_1123_DATE_TIME));
        params.put("number", widgetItem.getNum());

        ResponseEntity<ObjectNode> response = connector.invokeAPI(urlMethod, HttpMethod.POST, params);
        ArrayNode countCall = (ArrayNode) response.getBody().get("data");

        GeckOMeter geckOMeter = new GeckOMeter(widgetItem.getWidgetKey(), GraphType.STANDARD);
        geckOMeter.setMin("min", geckOMeterProps.getProperty("min"));
        geckOMeter.setMax("max", geckOMeterProps.getProperty("max"));
        geckOMeter.setCurrent(Integer.toString(countCall.size()));

        return geckOMeter;
    }

    private void initPros() {
        try {
            if (geckOMeterProps == null) {
                geckOMeterProps = new Properties();

                geckOMeterProps.load(ResourseHelper.getResourse(ResourseHelper.GECK_O_METER));

            }
        } catch (IOException ex) {
            Logger.getLogger(DerivedGeckOMeterin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
