/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.widgets;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import nl.pvanassen.geckoboard.api.Push;
import nl.pvanassen.geckoboard.api.json.common.GraphType;
import nl.pvanassen.geckoboard.api.widget.GeckOMeter;
import nl.pvanassen.geckoboard.api.widget.RAGNumbersOnly;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import ru.edance.onlinepbx.OnlinePBXConnector;
import ru.edance.onlinepbx.jaxb.model.WidgetItem;

/**
 *
 * @author ILMIR
 */
public class DerivedRAGNumbers extends BaseWidget {

    private OnlinePBXConnector connector;
    private static String urlMethod = "/history/search.json";
    private static Properties rangNumbersProps;

    public DerivedRAGNumbers() {
    }

    public DerivedRAGNumbers(OnlinePBXConnector connector) {
        this.connector = connector;
    }

    @Override
    public Push getPushObject(WidgetItem widgetItem) {
        RAGNumbersOnly widget = new RAGNumbersOnly(widgetItem.getWidgetKey());
        ZonedDateTime nowOfDay = ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS);
     
        String date_from = nowOfDay.minusDays(widgetItem.getNumbersOfday()).format(DateTimeFormatter.RFC_1123_DATE_TIME);
        String date_to = nowOfDay.plusSeconds(86400 - 1).format(DateTimeFormatter.RFC_1123_DATE_TIME);

        widget.setGreen("От 20 секунд", getGreenValue(widgetItem, date_from, date_to));
        widget.setAmber("До 20 секунд", getAmberValue(widgetItem, date_from, date_to));
        widget.setRed("Не успешные", getRedValue(widgetItem, date_from, date_to));
        return widget;
    }

    private int getGreenValue(WidgetItem widgetItem, String date_from, String date_to) {
        ObjectNode params = connector.getObjectMapper().valueToTree(new Object());
        params.put("billsec_from", "20");
        params.put("date_from", date_from);
        params.put("date_to", date_to);
        params.put("number", widgetItem.getNum());

        ResponseEntity<ObjectNode> response = connector.invokeAPI(urlMethod, HttpMethod.POST, params);
        ArrayNode countCall = (ArrayNode) response.getBody().get("data");
        return countCall.size();
    }

    private int getAmberValue(WidgetItem widgetItem, String date_from, String date_to) {
        ObjectNode params = connector.getObjectMapper().valueToTree(new Object());
        params.put("billsec_from", "6");
        params.put("billsec_to", "20");
        params.put("date_from", date_from);
        params.put("date_to", date_to);
      params.put("number", widgetItem.getNum());

        ResponseEntity<ObjectNode> response = connector.invokeAPI(urlMethod, HttpMethod.POST, params);
        ArrayNode countCall = (ArrayNode) response.getBody().get("data");
        return countCall.size();
    }

    private int getRedValue(WidgetItem widgetItem, String date_from, String date_to) {
        ObjectNode params = connector.getObjectMapper().valueToTree(new Object());
        params.put("billsec_to", "6");
        params.put("date_from", date_from);
        params.put("date_to", date_to);
        params.put("number", widgetItem.getNum());

        ResponseEntity<ObjectNode> response = connector.invokeAPI(urlMethod, HttpMethod.POST, params);
        ArrayNode countCall = (ArrayNode) response.getBody().get("data");
        System.err.println(countCall.toString());
        return countCall.size();
    }

}
