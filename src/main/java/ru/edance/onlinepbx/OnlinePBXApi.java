package ru.edance.onlinepbx;

/**
 * Created by code8 on 12/11/15.
 */

public interface OnlinePBXApi {
    AuthKey getAuthKey();
}
