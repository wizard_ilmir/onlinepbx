/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx;

import ru.edance.onlinepbx.utils.ResourseHelper;
import ru.edance.onlinepbx.jaxb.model.WidgetItems;
import ru.edance.onlinepbx.jaxb.model.WidgetItem;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import nl.pvanassen.geckoboard.api.Geckoboard;
import nl.pvanassen.geckoboard.api.Push;
import nl.pvanassen.geckoboard.api.json.common.GraphType;
import nl.pvanassen.geckoboard.api.widget.GeckOMeter;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import ru.edance.onlinepbx.jaxb.UnmarshalHelper;
import ru.edance.onlinepbx.widgets.BaseWidget;

/**
 *
 * @author ILMIR
 */
public class Main extends TimerTask {

    private OnlinePBXConnector connector;
    private Geckoboard geckoboard;
    private WidgetItems widgetItems;

    public Main() throws Exception {
        connector = new OnlinePBXConnector(null, null);

        Properties authProps = new Properties();
        authProps.load(ResourseHelper.getResourse(ResourseHelper.AUTH));
        geckoboard = new Geckoboard(authProps.getProperty("apiGeckoboard"));
        
        widgetItems = UnmarshalHelper.unmarshal(ResourseHelper.getResourse(ResourseHelper.WIDGET_ITEMS));

    }

    public static void main(String[] args) throws Exception {
        TimerTask timerTask = new Main();
        //running timer task as daemon thread
          Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(timerTask, 0, 30 * 1000);
        System.out.println("TimerTask started");
        Thread.currentThread().join();
    }

    @Override
    public void run() {
          for (WidgetItem item : widgetItems.getWidgetItems()) {
            
            new Thread(() -> {
                Push widget = BaseWidget.factoryWidget(connector, item);
                if (widget != null) {
                    try {
                        geckoboard.push(widget);
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }).start();
            
        }

    }
}
