/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.edance.onlinepbx.utils;

import java.io.InputStream;

/**
 *
 * @author ILMIR
 */
public final class ResourseHelper {

    public static String WIDGET_ITEMS = "WidgetItems.xml";
    public static String AUTH = "auth.properties";
    public static String GECK_O_METER = "GeckOMeter.properties";
    
    public static InputStream getResourse(String fileName){
        return ResourseHelper.class.getClassLoader().getResourceAsStream(fileName);
    }
}
